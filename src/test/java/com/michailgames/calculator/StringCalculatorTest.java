package com.michailgames.calculator;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;


public class StringCalculatorTest {

    @Test
    public void sumOfZeroNumbersIsZero() {
        assertThat(add("")).isEqualTo(0);
    }

    @Test
    public void sumOfOneNumberIsEqualToThatNumber() {
        assertThat(add("54")).isEqualTo(54);
    }

    @Test
    public void sumOfTwoNumbersIsEqualToAddingThem() {
        assertThat(add("3,7")).isEqualTo(10);
    }

    @Test
    public void anyAmountOfNumbersCanBeAdded() {
        assertThat(add("1,1,1,5,5,5,8")).isEqualTo(26);
    }

    @Test
    public void newLinesCanBeUsedAsDelimitersInsteadOfComma() {
        assertThat(add(String.format("1%n2,3"))).isEqualTo(6);
    }

    @Test
    public void customDelimiterCanBeUsedInsteadOfComma() {
        assertThat(add(String.format("//;%n1;2;4"))).isEqualTo(7);
    }

    @Test
    public void emptyInputWIthCustomDelimiterSumsToZero() {
        assertThat(add(String.format("//c%n"))).isEqualTo(0);
    }

    @Test
    public void newLineIsStillTreatedAsDelimiterWhenCustomDelimiterIsProvided() {
        assertThat(add(String.format("//;%n5;2%n4"))).isEqualTo(11);
    }

    @Test
    public void customDelimiterCannotBeInjectedAsRegularExpression() {
        assertThat(add(String.format("//.*%n222.*333"))).isEqualTo(555);
    }

    @Test
    public void exceptionIsThrownWhenAnyNegativeNumberIsGiven() {
        assertThat(assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> add("1,-2,3"))
                .withMessage("Negatives not allowed: -2"));
    }

    @Test
    public void exceptionMessageContainsListOfAllNegativeNumbers() {
        assertThat(assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> add("-1,-2,3,-4"))
                .withMessage("Negatives not allowed: -1,-2,-4"));
    }

    @Test
    public void numbersBiggerThanThousandAreIgnored() {
        assertThat(add("2,1001")).isEqualTo(2);
    }

    @Test
    public void thousandIsNotIgnored() {
        assertThat(add("2,1000")).isEqualTo(1002);
    }

    @Test
    public void delimitersCanBeLongerThanOneCharWhenEnclosedInSquareBrackets() {
        assertThat(add(String.format("//[***]%n1***2***3"))).isEqualTo(6);
    }

    @Test
    public void multipleDelimitersCanBeProvidedInSquareBrackets() {
        assertThat(add(String.format("//[**][^][&&&]%n2**3&&&4^1"))).isEqualTo(10);
    }

    private int add(String s) {
        return new StringCalculator().add(s);
    }
}
