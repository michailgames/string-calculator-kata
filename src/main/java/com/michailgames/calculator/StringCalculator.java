package com.michailgames.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class StringCalculator {

    private static final String DEFAULT_DELIMITER = ",";
    private static final String NEWLINE_DELIMITER = "\n";

    public int add(String numbers) {
        DelimiterAndNumbers input = parseInput(numbers);
        List<Integer> integers = input.toIntegers();
        checkForNegativeNumbers(integers);
        return sum(integers);
    }

    private DelimiterAndNumbers parseInput(String numbers) {
        if (numbers.startsWith("//") && numbers.contains(NEWLINE_DELIMITER)) {
            String[] headerAndNumbers = numbers.substring("//".length()).split(NEWLINE_DELIMITER, 2);
            String delimiter = headerAndNumbers[0];
            String rest = headerAndNumbers[1];
            return new DelimiterAndNumbers(parseDelimiters(delimiter, NEWLINE_DELIMITER), rest);
        } else {
            return new DelimiterAndNumbers(Arrays.asList(DEFAULT_DELIMITER, NEWLINE_DELIMITER), numbers);
        }
    }

    private List<String> parseDelimiters(String stringToParse, String defaultDelimiter) {
        final List<String> result = new ArrayList<>();
        result.add(defaultDelimiter);
        if (stringToParse.startsWith("[") && stringToParse.endsWith("]")) {
            result.addAll(parseMultipleDelimiters(stringToParse));
        } else {
            result.add(stringToParse);
        }
        return result;
    }

    private List<String> parseMultipleDelimiters(String stringToParse) {
        String[] delimiters = stringToParse.substring("[".length(), stringToParse.length() - "]".length())
                .split("]\\[");
        return Arrays.asList(delimiters);
    }

    private void checkForNegativeNumbers(List<Integer> integers) {
        List<Integer> negatives = integers.stream().filter(number -> number < 0).collect(toList());
        if (!negatives.isEmpty()) {
            throw new IllegalArgumentException("Negatives not allowed: " +
                    negatives.stream().map(String::valueOf).collect(joining(",")));
        }
    }

    private int sum(Collection<Integer> numbers) {
        return numbers.stream().mapToInt(Integer::intValue).sum();
    }

    private static class DelimiterAndNumbers {

        private final String delimiter;
        private final String numbers;

        private DelimiterAndNumbers(List<String> delimiters, String numbers) {
            this.delimiter = prepareDelimiterExpression(delimiters);
            this.numbers = numbers;
        }

        private String prepareDelimiterExpression(List<String> delimiters) {
            return delimiters.stream().map(Pattern::quote).collect(joining("|"));
        }

        List<Integer> toIntegers() {
            return getTokens().stream()
                    .map(Integer::parseInt)
                    .filter(number -> number <= 1000)
                    .collect(toList());
        }

        private List<String> getTokens() {
            return Arrays.stream(numbers.split(delimiter))
                    .filter(s -> !s.isEmpty())
                    .collect(toList());
        }
    }
}
